FROM ubuntu:16.04

LABEL maintainer "frank.intorp@techvikings.de"

RUN apt-get update && apt-get install -y curl autoconf automake libtool autoconf-archive pkg-config libpng12-dev libjpeg8-dev libtiff5-dev zlib1g-dev

RUN apt-get install -y git

RUN git clone https://github.com/DanBloomberg/leptonica.git \
  && cd leptonica/ \
  && ./autobuild  \
	&& ./configure --prefix=/usr/local  \
  && make install

RUN git clone --depth 1 https://github.com/tesseract-ocr/tesseract.git \
  && cd tesseract/ \
  && ./autogen.sh \
  && ./configure --enable-debug \
  && LDFLAGS="-L/usr/local/lib" CFLAGS="-I/usr/local/include" make \
  && make install \
  && ldconfig
WORKDIR /usr/local/share/tessdata

RUN curl -o deu.traineddata https://raw.githubusercontent.com/tesseract-ocr/tessdata/4.00/deu.traineddata
ENV TESSDATA_PREFIX /usr/local/share/tessdata

ENTRYPOINT ["tesseract"]
CMD ["-h"]
