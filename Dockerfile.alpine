FROM alpine:3.5

LABEL maintainer "frank.intorp@techvikings.de"

RUN apk update
RUN apk add --no-cache jpeg libpng tiff
RUN apk add --no-cache build-base gcc abuild binutils autoconf pkgconfig \
			automake libtool git jpeg-dev libpng-dev tiff-dev zlib-dev

RUN git clone https://github.com/peti/autoconf-archive.git \
  && mkdir -p /usr/share/aclocal \
  && cp -a autoconf-archive/m4/* /usr/share/aclocal \
  && rm -rf autoconf-archive \
  && mkdir -p /build \
  && cd /build

RUN git clone https://github.com/DanBloomberg/leptonica.git \
  && cd leptonica/ \
  && ./autobuild  \
	&& ./configure --prefix=/usr/local  \
  && make install


RUN git clone --depth 1 https://github.com/tesseract-ocr/tesseract.git
  RUN cd tesseract/ \
  && ./autogen.sh \
	&& LIBLEPT_HEADERSDIR=/usr/local/include \
	&& ./configure --prefix=/usr/local/ --with-extra-libraries=/usr/local/lib
  RUN cd tesseract/ && make install
  ##RUN cd tesseract/ && ldconfig

RUN cd /usr/local/share/tessdata && \
  curl -o deu.traineddata https://raw.githubusercontent.com/tesseract-ocr/tessdata/4.00/deu.traineddata

ENV TESSDATA_PREFIX /usr/local/share/tessdata

ENV PATH /usr/local/bin:$PATH
ENV LD_LIBRARY_PATH /usr/local/lib

ENTRYPOINT ["tesseract"]
CMD ["-h"]
